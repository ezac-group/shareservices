import splunk.Intersplunk
import ConfigParser

args, kwargs = splunk.Intersplunk.getKeywordsAndOptions()

if len(args) == 4:
    config = ConfigParser.RawConfigParser()
    config.optionxform = str

    config.add_section(args[0])
    # Add items in reverse order.
    # In file they will be in correct order
    config.set(args[0], 'homePath', '$SPLUNK_DB/{}/db'.format(args[0].lower()))
    config.set(args[0], 'coldPath', '$SPLUNK_DB/{}/colddb'.format(args[0].lower()))
    config.set(args[0], 'thawedPath', '$SPLUNK_DB/{}/thaweddb'.format(args[0].lower()))
    config.set(args[0], 'maxTotalDataSizeMB', args[1])
    config.set(args[0], 'frozenTimePeriodInSecs', args[2])

    if int(args[3]) > 0:
        config.set(args[0], 'enableTsidxReduction', 'true')
        config.set(args[0], 'timePeriodInSecBeforeTsidxReduction', args[3])

    config.set(args[0], 'repFactor', 'auto')

    # Append config to file
    with open('../../../master-apps/_cluster/local/indexes.conf', 'ab') as configfile:
        config.write(configfile)
